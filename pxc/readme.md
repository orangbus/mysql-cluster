# pxc集群
一个一个的启动
## 1、首先启动master
```bash
docker-compose up -d master
```
## 2、等待 master 能够正常连接在启动第二个
```bash
dc up -d node1
```
## 3、等待能够正常连接在启动第三个
```bash
dc up -d node2
```

进入 master 数据库执行sql语句查看集群
```mysql
show status like 'wsrep_cluster%'
```

其中`wsrep_cluster_size` 的个数就是集群的数量
