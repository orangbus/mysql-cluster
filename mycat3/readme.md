## 重新 build mycat
```bash
dc build --no-cache mycat
```

## 在两个master主节点添加一个没有权限的用户，用于haproxy的心跳检测
```sql
CREATE USER 'haproxy'@'%' identified BY '';
```

## todo 使用haproxy监控mycat

> https://www.cnblogs.com/hk315523748/p/6094656.html

简单理解安装一个 xinetd 来守护一个进程，这个进程的内容可以是一个脚本，用来检测 mycat 的状态，然后 haproxy 监听 xinetd 守护的进程达到检测mycat的转台

暂时没时间弄，以后实际用到了在说吧，如果同学做了也可以分享一下。