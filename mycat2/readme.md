# mycat 中间件 mysql读写分离

## 主从节点
```bash
master 3336
slave 3337
```
> 如果启动失败，授权相应文件夹权限
```bash
sudo chmod -R 777 config data
```
## mycat 连接

port：8066 //映射的端口:8076
password 主库的数据库账户密码

## 踩坑指南

