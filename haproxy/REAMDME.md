 # haproxy + pxc
 顺序启动，数据库能正常连接在启动下一mysql容器，否则各种未知bug
 ```bash
dc up -d master

dc up -d node1

dc up -d node2

dc up -d haproxy
```

## 集群查看
登录master节点,进行sql查询
```bash
show status like 'wsrep_cluster%'
```

如果查询出来的结果中的`wsrep_cluster_size` 的值为：3 ，pxc集群就搭建成功了

## 添加一个没有权限的用户，用于haproxy的心跳检测
```sql
CREATE USER 'haproxy'@'%' identified BY '';
```

## 存在未解决的问题
Q1:haproxy无法进行mysql心跳检测

```bash
# haproxy日志

 [WARNING] 199/065840 (8) : Server mysql_proxy/master is DOWN, reason: Layer4 connection problem, info: "Connection refused", check duration: 0ms. 2 active and 0 backup servers left. 0 sessions
 active, 0 requeued, 0 remaining in queue.
haproxy_1  | [WARNING] 199/065841 (8) : Server mysql_proxy/node1 is DOWN, reason: Layer4 connection problem, info: "Connection refused", check duration: 0ms. 1 active and 0 backup servers left. 0 sessions
active, 0 requeued, 0 remaining in queue.
haproxy_1  | [WARNING] 199/065842 (8) : Server mysql_proxy/node2 is DOWN, reason: Layer4 connection problem, info: "Connection refused", check duration: 0ms. 0 active and 0 backup servers left. 0 sessions
active, 0 requeued, 0 remaining in queue.
haproxy_1  | [ALERT] 199/065842 (8) : proxy 'mysql_proxy' has no server available!
```

有没有路过的大佬帮忙看一下是什么问题？
