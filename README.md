# mysql集群练习
测试环境要求：
 - docker: 20.10.6
 - docker-composer: 1.28.4

一直使用的是 manjaro 系统，其它linux发行版运行理论是没啥问题的。

相应的集群方案放在对应的文件夹目录中,使用和运行的相关方法都在对应的文件目录下。

gitee：https://gitee.com/orangbus/mysql-cluster.git

## mysql: 主从复制

## mycat: 数据库分片

## haproxy：数据库负载均衡

## pxc：数据强一致性

## 感谢
关于dockerfile有些不懂的地方，参考了laradock